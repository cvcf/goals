(in-package #:rad-user)

(define-module #:goals/ui
  (:use #:cl
        #:radiance
        #:goals))
