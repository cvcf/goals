# Goals

A simple application to collect data from users regarding whether they've met
their goals.

## License

Copyright © 2021 Cameron V Chaparro <cameron@cameronchaparro.com>

This work is licensed under the terms of the [GNU GPL version 3][1]


[1]: https://www.gnu.org/licenses/gpl-3.0.en.html
