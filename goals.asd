(defsystem #:goals
  :description "Collect data from users regarding whether they've met their goals."
  :version "0.0.1"
  :maintainer "Cameron V Chaparro <cameron@cameronchaparro.com>"
  :author "Cameron V Chaparro <cameron@cameronchaparro.com>"
  :license "GPL3"
  :serial t
  :pathname "src/"
  :components ((:file "goals")))

(defsystem #:goals/ui
  :description "Collect data from users regarding whether they've met their goals."
  :version "0.0.1"
  :maintainer "Cameron V Chaparro <cameron@cameronchaparro.com>"
  :author "Cameron V Chaparro <cameron@cameronchaparro.com>"
  :license "GPL3"
  :defsystem-depends-on (:radiance)
  :class "radiance:virtual-module"
  :depends-on (:goals)
  :serial t
  :pathname "src/"
  :components ((:module "ui"
                :components ((:file "module")
                             (:file "api")
                             (:file "frontend")))))
